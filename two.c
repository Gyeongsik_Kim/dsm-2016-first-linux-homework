#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
void check(char*);

int main(int argc, char* argv[]){
	if(argc != 2){
		fprintf(stderr, "USE : <PROGRAM NAME> <PATH>\n");
		exit(EXIT_FAILURE);
	}else
		check(argv[1]);
	return 0;	
}


void check(char* path){

	struct stat file;
	lstat(path, &file);
        
	if(S_ISLNK(file.st_mode))
		fprintf(stdout, "That's symbolic link\n");
	else if(file.st_nlink > 1)
		fprintf(stdout, "That's hard link\n");
	else{
		fprintf(stderr, "That's not link\n");
		exit(EXIT_FAILURE);
	}
}
