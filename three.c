#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <dirent.h>

#define MAX 1024
 
int indent = 0, fileCount, dirCount;
char* cdir, *now;
void Scan(char*);

int main (int argc, char *argv[]){
	cdir = calloc(sizeof(char), MAX);
	now = calloc(sizeof(char), MAX);
	if(argc == 1){
		getcwd(now, MAX);
		Scan(now);
	}else{
		fprintf(stderr, "<USE> : <Program>\n");
		exit(EXIT_FAILURE);
	}
	return 0;
}

void Scan(char *path){
	DIR *drp;
	struct dirent *list;
	struct stat state;
	int i;

	if((drp = opendir(path)) == NULL){
		fprintf(stderr, "Can't Open DIR\n");
		exit(EXIT_FAILURE);
	}
	
	chdir(path);
	if(strcmp(now, path) == 0)
		fprintf(stdout, "\n\nPath : %s\n", now);
	else
		fprintf(stdout, "\n\nPath : %s/%s\n",now, path);
	while((list=readdir(drp))!=NULL){
		lstat(list->d_name, &state);
		if(S_ISDIR(state.st_mode)){
			if(strcmp(".", list->d_name) == 0 || strcmp("..", list->d_name) == 0){
				fprintf(stdout, "%s\n", list->d_name);
				continue;
			}
	
			fprintf(stdout, "%s\n", list->d_name);
			Scan(list->d_name);
		}
	}
	chdir("..");
	closedir(drp);
}
