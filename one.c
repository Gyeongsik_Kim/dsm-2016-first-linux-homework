#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
void check(char*);

int main(int argc, char* argv[]){
	if(argc != 2){
		fprintf(stderr, "USE : <PROGRAM NAME> <PATH>\n");
		exit(EXIT_FAILURE);
	}else
		check(argv[1]);
			
}


void check(char* path){
	if(strncmp(path, "/", sizeof("/") > 0)){ 
		fprintf(stderr, "Not absolute path\n");
		fprintf(stderr, "Your input path : %s\n", path);
		exit(EXIT_FAILURE);
	}
	
	if(access(path, F_OK) == 0)
		fprintf(stdout, "Path that does exist\n");
	else
		fprintf(stdout, "Path that doesn't exist\n");

}
